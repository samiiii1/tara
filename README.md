# Auth service

This project was bootstrapped with express.

## Available Scripts

In the project directory, you can run:

### `npm run db:migrate`

to making your database setup and update after every modify on schema file , you must run this cmd first

### `npm run dev`

To start the app in dev mode.\

### `npm run start`

For production mode

### `docker-compose up -d`

For one-command start mode
