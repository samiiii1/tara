import controller from "../http/app/controllers/auth";
import validator from "../http/app/requests/auth.request.validator";
import expressValidatorResult from "../middlewares/express.validator.result";

import { Router } from "express";
const router = Router();
router.post(
  "/api/v1/login",
  validator.login,
  expressValidatorResult,
  controller.login
);

router.post(
  "/api/v1/register",
  validator.register,
  expressValidatorResult,
  controller.register
);
export default router;
