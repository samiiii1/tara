import controller from "../http/app/controllers/admin";
import validator from "../http/app/requests/admin.request.validator";
import expressValidatorResult from "../middlewares/express.validator.result";

import requireAuth from "../middlewares/require.auth";
import acl, { Role } from "../middlewares/acl";

import { Router } from "express";
const router = Router();
router.post(
  "/api/v1/topic",
  requireAuth(),
  acl(Role.admin),
  validator.insertTopic,
  expressValidatorResult,
  controller.insertTopic
);
router.post(
  "/api/v1/section",
  requireAuth(),
  acl(Role.admin),
  validator.insertSection,
  expressValidatorResult,
  controller.insertSection
);
router.post(
  "/api/v1/question",
  requireAuth(),
  acl(Role.admin),
  validator.insertQuestion,
  expressValidatorResult,
  controller.insertQuestion
);

router.patch(
  "/api/v1/question",
  requireAuth(),
  acl(Role.admin),
  validator.updateQuestionStatus,
  expressValidatorResult,
  controller.updateQuestionStatus
);

export default router;
