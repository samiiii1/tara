import controller from "../http/app/controllers/user";
import validator from "../http/app/requests/user.request.validator";
import expressValidatorResult from "../middlewares/express.validator.result";

import requireAuth from "../middlewares/require.auth";
import acl, { Role } from "../middlewares/acl";
import { Router } from "express";
const router = Router();
router.get(
  "/api/v1/question",
  validator.view,
  expressValidatorResult,
  controller.getQuestion
);
router.get("/api/v1/popular-question", controller.getPopularQuestion);
router.post(
  "/api/v1/like",
  requireAuth(),
  acl(Role.user),
  validator.like,
  expressValidatorResult,
  controller.like
);
router.post(
  "/api/v1/disslike",
  requireAuth(),
  acl(Role.user),
  validator.disslike,
  expressValidatorResult,
  controller.disslike
);

export default router;
