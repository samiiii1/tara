import userRouter from "./user";
import authRouter from "./auth";
import adminRouter from "./admin";

export default [userRouter, authRouter, adminRouter];
