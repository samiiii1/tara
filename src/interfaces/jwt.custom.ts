import { JwtPayload } from "jsonwebtoken";

export interface Payload {
  userId: number;
  email: string;
}

export enum ApproachMethod {
  "accessToken<=>refreshToken" = "accessToken<=>refreshToken",
  "singleToken" = "singleToken",
}

export interface GenResponse {
  accessToken?: string;
  refreshToken?: string;
  singleToken?: string;
  expireTime?: string;
}

export interface VerifyResponse extends JwtPayload {
  isSafe: boolean;
  payload?: Payload;
}
