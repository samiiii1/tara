// export enum ResponseCode {
//   "insert" = 100
// }

export interface Success {
  output: any
  status: "succeeded"
}

export interface TraceSuccess {
  output: any
  status: "succeeded"
  trackId: number
}

export interface UnSuccess {
  output: any
  status: "unsucceeded"
}

export interface TraceUnSuccess {
  output: any
  status: "unsucceeded"
  trackId: number
}
