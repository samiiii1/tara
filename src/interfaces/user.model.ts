export enum Role {
  "admin" = 1,
  "user" = 2,
}

export interface User {
  id: number;
  email: string;
  role?: Role;
  password: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}
