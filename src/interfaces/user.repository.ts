export interface loginParam {
  email: string;
  password: string;
}

export interface likeParam {
  questionId: number;
  userId: number;
}

export interface disslikeParam {
  questionId: number;
  userId: number;
}

export interface viewParam {
  questionId: number;
  userId?: number;
}
