import { NextFunction, Request, Response } from "express";
import CustomError from "../utils/error.custom";
import JwtCustom from "../utils/jwt.custom";
import { Role } from "../interfaces/user.model";

const acl =
  (role: Role) => async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { roleId } = req.currentUser;
      if (role != roleId)
        throw new CustomError("Role Access Denied", 403, [
          { message: "Forbidden", param: "authorization" },
        ]);

      next();
    } catch (error: any) {
      console.log(error.message);
      next(error);
    }
  };

export default acl;
export { Role };
