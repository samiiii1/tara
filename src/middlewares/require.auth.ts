import { NextFunction, Request, Response } from "express";
import CustomError from "../utils/error.custom";
import JwtCustom from "../utils/jwt.custom";
import prisma from "../utils/db.client";

const requireAuth =
  (mandatory?: boolean) =>
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { authorization } = req.headers;
      if (!authorization)
        throw new CustomError("Authorization Error", 401, [
          { message: "Missed", param: "authorization" },
        ]);

      const token = authorization.split("Bearer ")[1];

      if (!token)
        throw new CustomError("Authorization Error", 401, [
          { message: "Malformed Bearer Token", param: "authorization" },
        ]);

      const verify = new JwtCustom().verify(token, {});

      if (!verify.isSafe)
        throw new CustomError("Authorization Error", 401, [
          { message: "Invalid Token", param: "authorization" },
        ]);
      const user = await prisma.cli.user.findFirst({
        where: { id: verify.payload!.userId },
      });
      req.currentUser = user;

      next();
    } catch (error: any) {
      console.log(error.message);
      next(error);
    }
  };

export default requireAuth;
