import { NextFunction, Request, Response } from "express";
import CustomError from "../utils/error.custom";
import JwtCustom from "../utils/jwt.custom";
import prisma from "../utils/db.client";

const errorHandler = async (
  error: CustomError,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.status(error.status || 500).json({
    output: error.message,
    detail: error.detail || [],
    status: "unsucceeded",
    code: error.statusCode || 500,
  });
};

export default errorHandler;
