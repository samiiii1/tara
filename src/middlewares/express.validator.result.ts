import { Request, Response, NextFunction } from "express";
import CustomError from "../utils/error.custom";
import { validationResult } from "express-validator";
export default (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new CustomError("Validation Error", 422, errors.array());
  } else {
    return next();
  }
};
