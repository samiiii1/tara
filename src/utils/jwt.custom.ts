import jwt, { SignOptions, VerifyOptions } from "jsonwebtoken";

import {
  Payload,
  ApproachMethod,
  GenResponse,
  VerifyResponse,
} from "../interfaces/jwt.custom";

class JwtCustom {
  private _secretKey: string = process.env.JWT_SECRET!;

  gen(
    payload: Payload,
    approachMethod: ApproachMethod,
    option: SignOptions[]
  ): GenResponse {
    const result: GenResponse = {};
    if (
      approachMethod === ApproachMethod["accessToken<=>refreshToken"] &&
      option.length === 2
    ) {
      result.refreshToken = jwt.sign(payload, this._secretKey, option[0]);
      result.accessToken = jwt.sign(payload, this._secretKey, option[1]);
      result.expireTime = option[1].expiresIn as string;
    }
    if (
      approachMethod === ApproachMethod["singleToken"] &&
      option.length === 1
    ) {
      result.singleToken = jwt.sign(payload, this._secretKey, option[0]);
      result.expireTime = option[0].expiresIn as string;
    }
    return result;
  }

  verify(token: string, option: VerifyOptions): VerifyResponse {
    try {
      const verify = jwt.verify(token, this._secretKey, option) as Payload;

      return {
        ...verify,
        isSafe: true,
        payload: {
          email: verify!.email,
          userId: verify!.userId,
        },
      };
    } catch (error) {
      return { isSafe: false };
    }
  }
}
export { ApproachMethod, GenResponse };
export default JwtCustom;
