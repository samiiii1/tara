import { PrismaClient } from "@prisma/client";
import seedData from "../constants/seed.data";
import Hash from "./hash.custom";
import CustomError from "./error.custom";
import { Role } from "../interfaces/user.model";

class Seed {
  constructor(public cli: PrismaClient) {
    this.cli = cli;
  }

  private async seedRole(): Promise<void | never> {
    try {
      const checkDublicate = await this.cli.role.findFirst();

      if (!checkDublicate) {
        for (let role of seedData.roles) {
          await this.cli.role.create({
            data: {
              name: role.name,
            },
          });
        }
      }
    } catch (error) {
      throw new CustomError(error.message, 500);
    }
  }

  private async seedQuestionOperator(): Promise<void | never> {
    try {
      const checkDublicate = await this.cli.questionOperator.findFirst();

      if (!checkDublicate) {
        for (let operator of seedData.questionOperators) {
          await this.cli.questionOperator.create({
            data: {
              name: operator.name,
            },
          });
        }
      }
    } catch (error) {
      throw new CustomError(error.message, 500);
    }
  }

  private async seedUser(): Promise<void | never> {
    try {
      const checkDublicate = await this.cli.user.findFirst();

      if (!checkDublicate) {
        await this.cli.user.create({
          data: {
            name: seedData.admin.name,
            email: seedData.admin.email,
            roleId: Role.admin,
            password: new Hash(seedData.admin.password).sha256(),
          },
        });

        await this.cli.user.create({
          data: {
            name: seedData.user.name,
            email: seedData.user.email,
            roleId: Role.user,
            password: new Hash(seedData.user.password).sha256(),
          },
        });
      }
    } catch (error) {
      throw new CustomError(error.message, 500);
    }
  }

  private async seedSection(): Promise<number | void | never> {
    try {
      const checkDublicate = await this.cli.section.findFirst();

      if (!checkDublicate) {
        const section = await this.cli.section.create({
          data: {
            name: seedData.section.name,
          },
        });
        return section.id;
      }
    } catch (error) {
      throw new CustomError(error.message, 500);
    }
  }

  private async seedTopic(sectionId: number): Promise<number | never> {
    try {
      const topic = await this.cli.topic.create({
        data: {
          sectionId,
          name: seedData.topic.name,
        },
      });
      return topic.id;
    } catch (error) {
      throw new CustomError(error.message, 500);
    }
  }

  private async seedQuestion(topicId: number): Promise<void | never> {
    try {
      await this.cli.question.create({
        data: {
          topicId,
          question: seedData.question.question,
          answer: seedData.question.answer,
        },
      });
    } catch (error) {
      throw new CustomError(error.message, 500);
    }
  }

  public async bootstrap() {
    await this.seedRole();
    await this.seedQuestionOperator();
    await this.seedUser();
    const sectionId = await this.seedSection();
    if (sectionId) {
      const topicId = await this.seedTopic(sectionId);
      await this.seedQuestion(topicId);
    }

    console.log("seeding the database has been checked");
  }
}

export default Seed;
