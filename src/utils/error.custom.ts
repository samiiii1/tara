export default class CustomErrorWithDetail extends Error {
  //add this line for default axios status can access with statusCode keyword
  public statusCode: number;
  constructor(
    public message: string,
    public status: number,
    public detail?: any[]
  ) {
    super(message);
    this.statusCode = status;
    Object.setPrototypeOf(this, CustomErrorWithDetail.prototype);
  }
}
