import crypto from "crypto";

class Hash {
  constructor(public data: string) {
    this.data = data;
  }

  public sha256(): string {
    const hash = crypto.createHash("sha256").update(this.data).digest("hex");
    return hash;
  }

  static compare(unHashedParam: string, hashedParam: string): boolean {
    const unHashedParamHash = new this(unHashedParam).sha256();
    return unHashedParamHash === hashedParam;
  }
}

export default Hash;
