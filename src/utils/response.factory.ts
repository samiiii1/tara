import {
  Success,
  TraceSuccess,
  TraceUnSuccess,
  UnSuccess,
} from "../interfaces/response.factory";
export enum ResponseType {
  "success" = "success",
  "trace_success" = "trace_success",
  "unsuccess" = "unsuccess",
  "trace_unsuccess" = "trace_unsuccess",
}

abstract class Response {
  abstract createResponse(
    output: any,
    trackId?: number
  ): Success | TraceSuccess | TraceUnSuccess | UnSuccess;
}

class SuccessResponse extends Response {
  createResponse(output: any): Success {
    return { output, status: "succeeded" };
  }
}

class TraceSuccessResponse extends Response {
  createResponse(output: any, trackId: number): TraceSuccess {
    return { output, status: "succeeded", trackId };
  }
}
class UnSuccessResponse extends Response {
  createResponse(output: any): UnSuccess {
    return { output, status: "unsucceeded" };
  }
}
class TraceUnSuccessResponse extends Response {
  createResponse(output: any, trackId: number): TraceUnSuccess {
    return { output, status: "unsucceeded", trackId };
  }
}

class ResponseFactory {
  private response!: Response;
  public output: any;
  public trackId?: number;

  constructor(type: ResponseType, output: any, trackId?: number) {
    this.output = output;
    this.trackId = trackId;

    switch (type) {
      case ResponseType.success:
        this.response = new SuccessResponse();
        break;
      case ResponseType.trace_success:
        this.response = new TraceSuccessResponse();
        break;
      case ResponseType.trace_unsuccess:
        this.response = new UnSuccessResponse();
        break;
      case ResponseType.unsuccess:
        this.response = new TraceUnSuccessResponse();
        break;
      default:
        break;
    }
  }

  createResponse() {
    return this.response.createResponse(this.output, this.trackId);
  }
}

export default ResponseFactory;
