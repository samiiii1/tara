import { Request } from "express";
import User from "../../interfaces/user.model";

declare global {
  declare namespace Express {
    export interface Request {
      currentUser: User;
    }
  }
}
