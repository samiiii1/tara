import "./utils/env.setter";
import express, { Express, json } from "express";
import cors from "cors";
import routes from "./routes/index";
import errorHandler from "./middlewares/error.handler";

class App {
  private _app: Express;
  constructor() {
    this._app = express();
  }
  get app() {
    return this._app;
  }

  public bootstrap() {
    this._app.use(json());
    this._app.use(cors());

    for (let route of routes) {
      this._app.use(route);
    }
    this._app.use(errorHandler);
  }
}

const appInstance = new App();
export default appInstance;
