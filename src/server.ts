import appInstance from "./app";
import prisma from "./utils/db.client";
import seed from "./utils/db.seed";

start();
async function start() {
  await prisma.cli.$connect();
  console.log("prisma connected to mysql");
  new seed(prisma.cli).bootstrap();
  appInstance.bootstrap();
  appInstance.app.listen(process.env.PORT || 8000, () => {
    console.log(`server is running on ${process.env.PORT || 8000} `);
  });
}
