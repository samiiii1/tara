export default {
  getScore: { path: "/api/v1/user/score", method: "GET" },
  login: { path: "/api/v1/auth/login", method: "POST" },
};
