export default {
  section: {
    name: "deposit and withdraw section",
  },
  topic: {
    name: "how our deposit flow work",
  },
  question: {
    question: "can i deposit with USDT",
    answer: "yes , we currently support crypto deposit",
  },
  user: {
    name: "test",
    email: "test@gmail.com",
    password: "12344321",
  },
  admin: {
    name: "admin",
    email: "admin@gmail.com",
    password: "12344321",
  },

  //the sequent of this array is important
  roles: [
    {
      name: "admin",
    },
    {
      name: "user",
    },
  ],

  questionOperators: [
    {
      name: "like",
    },
    {
      name: "disslike",
    },
    {
      name: "view",
    },
  ],
};
