import { PrismaClient } from "@prisma/client";
import {
  loginParam,
  likeParam,
  disslikeParam,
  viewParam,
} from "../../interfaces/user.repository";
import { QuestionOperator } from "../../interfaces/question.model";
import CustomError from "../../utils/error.custom";
import CustomHash from "../../utils/hash.custom";
import CustomJWT, { ApproachMethod, GenResponse } from "../../utils/jwt.custom";

class UserRepository {
  constructor(public cli: PrismaClient) {
    this.cli = cli;
  }
  async login(
    param: loginParam
  ): Promise<{ token: GenResponse; user: any } | never> {
    const targetUser = await this.cli.user.findFirst({
      where: { email: param.email },
    });
    if (!targetUser) throw new CustomError("Invalid Credentials", 400);
    const passCompare = CustomHash.compare(param.password, targetUser.password);
    if (!passCompare) throw new CustomError("Invalid Credentials", 400);
    // delete targetUser.password;
    const token = new CustomJWT().gen(
      { email: targetUser.email, userId: targetUser.id },
      ApproachMethod.singleToken,
      [
        {
          expiresIn: "10m",
        },
      ]
    );
    return { token, user: targetUser };
  }

  async like(param: likeParam): Promise<any | never> {
    const questionDetail = await this.cli.questionDetail.findFirst({
      where: {
        questionId: param.questionId,
        userId: param.userId,
        QuestionOperatorId: QuestionOperator.like,
      },
    });
    if (questionDetail)
      throw new CustomError("Already Liked This Content", 400);

    const like = await this.cli.questionDetail.create({
      data: {
        questionId: param.questionId,
        userId: param.userId,
        QuestionOperatorId: QuestionOperator.like,
      },
    });

    await this.cli.question.update({
      where: { id: param.questionId },
      data: {
        like: { increment: 1 },
      },
    });

    return like;
  }

  async disslike(param: disslikeParam): Promise<any | never> {
    const questionDetail = await this.cli.questionDetail.findFirst({
      where: {
        questionId: param.questionId,
        userId: param.userId,
        QuestionOperatorId: QuestionOperator.disslike,
      },
    });
    if (questionDetail)
      throw new CustomError("Already DissLiked This Content", 400);

    const disslike = await this.cli.questionDetail.create({
      data: {
        questionId: param.questionId,
        userId: param.userId,
        QuestionOperatorId: QuestionOperator.disslike,
      },
    });

    await this.cli.question.update({
      where: { id: param.questionId },
      data: {
        disslike: { increment: 1 },
      },
    });

    return disslike;
  }

  async view(param: viewParam): Promise<any | never> {
    const view = await this.cli.questionDetail.create({
      data: {
        questionId: param.questionId,
        userId: param.userId,
        QuestionOperatorId: QuestionOperator.view,
      },
    });

    await this.cli.question.update({
      where: { id: param.questionId },
      data: {
        view: { increment: 1 },
      },
    });
    return view;
  }
}

export default UserRepository;
