import { check } from "express-validator";

const validation = {
  like: [
    check("questionId", "Invalid questionId")
      .notEmpty()
      .withMessage("questionId must be provided")
      .bail()
      .trim()
      .isNumeric()
      .withMessage("questionId must be number")
      .bail(),
  ],
  disslike: [
    check("questionId", "Invalid questionId")
      .notEmpty()
      .withMessage("questionId must be provided")
      .bail()
      .trim()
      .isNumeric()
      .withMessage("questionId must be number")
      .bail(),
  ],

  view: [
    check("questionId", "Invalid questionId")
      .notEmpty()
      .withMessage("questionId must be provided")
      .bail()
      .trim()
      .isNumeric()
      .withMessage("questionId must be number")
      .bail(),
  ],
};

export default validation;
