import { check } from "express-validator";

const validation = {
  insertSection: [
    check("name", "Invalid name")
      .notEmpty()
      .withMessage("name must be provided")
      .bail()
      .trim()
      .isString()
      .withMessage("name must be string")
      .bail(),
  ],
  insertTopic: [
    check("name", "Invalid name")
      .notEmpty()
      .withMessage("name must be provided")
      .bail()
      .trim()
      .isString()
      .withMessage("name must be string")
      .bail(),
    check("sectionId", "Invalid sectionId")
      .notEmpty()
      .withMessage("sectionId must be provided")
      .bail()
      .trim()
      .isNumeric()
      .withMessage("sectionId must be number")
      .bail(),
  ],
  insertQuestion: [
    check("question", "Invalid question")
      .notEmpty()
      .withMessage("question must be provided")
      .bail()
      .trim()
      .isString()
      .withMessage("question must be string")
      .bail(),
    check("answer", "Invalid answer")
      .notEmpty()
      .withMessage("answer must be provided")
      .bail()
      .trim()
      .isString()
      .withMessage("answer must be string")
      .bail(),
    check("topicId", "Invalid topicId")
      .notEmpty()
      .withMessage("topicId must be provided")
      .bail()
      .trim()
      .isNumeric()
      .withMessage("topicId must be number")
      .bail(),
  ],
  updateQuestionStatus: [
    check("questionId", "Invalid questionId")
      .notEmpty()
      .withMessage("questionId must be provided")
      .bail()
      .trim()
      .isNumeric()
      .withMessage("questionId must be number")
      .bail(),
    check("status", "Invalid status")
      .notEmpty()
      .withMessage("status must be provided")
      .bail()
      .trim()
      .isBoolean()
      .withMessage("status must be boolean")
      .bail(),
  ],
};

export default validation;
