import { check } from "express-validator";

const validation = {
  login: [
    check("password", "Invalid password")
      .notEmpty()
      .withMessage("password must be provided")
      .bail()
      .trim()
      .isString()
      .withMessage("password must be string")
      .bail().isLength({min:4}).withMessage("password must have 4 or more character"),
      check("email", "Invalid email")
      .notEmpty()
      .withMessage("email must be provided")
      .bail()
      .trim()
      .isString()
      .withMessage("email must be string")
      .bail(),
  ],
  register: [
    check("password", "Invalid password")
    .notEmpty()
    .withMessage("password must be provided")
    .bail()
    .trim()
    .isString()
    .withMessage("password must be string")
    .bail().isLength({min:4}).withMessage("password must have 4 or more character"),
    check("email", "Invalid email")
    .notEmpty()
    .withMessage("email must be provided")
    .bail()
    .trim()
    .isString()
    .withMessage("email must be string")
    .bail(),
    check("name", "Invalid name")
    .notEmpty()
    .withMessage("name must be provided")
    .bail()
    .trim()
    .isString()
    .withMessage("name must be string")
    .bail(),
  ],


};

export default validation;
