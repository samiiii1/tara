import { Request, Response, NextFunction } from "express";
import prisma from "../../../utils/db.client";
import RF, { ResponseType } from "../../../utils/response.factory";
import User from "../../services/user.repository";
import CustomHash from "../../../utils/hash.custom";
import { Role } from "../../../interfaces/user.model";
import CustomError from "../../../utils/error.custom";

export default {
  login: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { email, password } = req.body;
      const token = await new User(prisma.cli).login({ email, password });
      const response = new RF(ResponseType.success, token).createResponse();
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },
  register: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { email, name, password } = req.body;
      const checkDub = await prisma.cli.user.findFirst({ where: { email } });
      if (checkDub) throw new CustomError("Email Already Registered", 400);
      const user = await prisma.cli.user.create({
        data: {
          name,
          email,
          password: new CustomHash(password).sha256(),
          roleId: Role.user,
        },
      });

      const response = new RF(ResponseType.success, user).createResponse();
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },
};
