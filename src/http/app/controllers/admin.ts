import { Request, Response, NextFunction } from "express";
import prisma from "../../../utils/db.client";
import RF, { ResponseType } from "../../../utils/response.factory";

export default {
  insertSection: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { name } = req.body;
      const section = await prisma.cli.section.create({
        data: {
          name,
        },
      });

      const response = new RF(ResponseType.success, section).createResponse();
      res.status(201).json(response);
    } catch (error) {
      next(error);
    }
  },

  insertTopic: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { name, sectionId } = req.body;

      const topic = await prisma.cli.topic.create({
        data: {
          name,
          sectionId: +sectionId,
        },
      });

      const response = new RF(ResponseType.success, topic).createResponse();
      res.status(201).json(response);
    } catch (error) {
      next(error);
    }
  },

  insertQuestion: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { question, answer, topicId } = req.body;
      const questionData = await prisma.cli.question.create({
        data: {
          answer,
          question,
          topicId: +topicId,
        },
      });

      const response = new RF(
        ResponseType.success,
        questionData
      ).createResponse();
      res.status(201).json(response);
    } catch (error) {
      next(error);
    }
  },

  updateQuestionStatus: async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const { questionId, status } = req.body;

      const question = await prisma.cli.question.update({
        where: { id: +questionId },
        data: {
          status: status == "true" ? true : false,
        },
      });

      const response = new RF(ResponseType.success, question).createResponse();
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },
};
