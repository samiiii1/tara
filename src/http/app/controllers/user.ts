import { Request, Response, NextFunction } from "express";
import prisma from "../../../utils/db.client";
import RF, { ResponseType } from "../../../utils/response.factory";
import User from "../../services/user.repository";

export default {
  like: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { questionId } = req.body;
      const like = await new User(prisma.cli).like({
        questionId: +questionId,
        userId: req.currentUser.id,
      });
      const response = new RF(ResponseType.success, like).createResponse();
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },

  disslike: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { questionId } = req.body;
      const disslike = await new User(prisma.cli).disslike({
        questionId: +questionId,
        userId: req.currentUser.id,
      });
      const response = new RF(ResponseType.success, disslike).createResponse();
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },

  getQuestion: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { questionId } = req.query;

      const question = await prisma.cli.question.findFirst({
        where: { id: +(questionId as string), status: true },
      });
      await new User(prisma.cli).view({
        questionId: +(questionId as string),
        userId: req.currentUser?.id,
      });
      const response = new RF(ResponseType.success, question).createResponse();
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },

  getPopularQuestion: async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const question = await prisma.cli.question.findMany({
        where: { status: true },
        take: 10,
        orderBy: { view: "desc" },
      });

      const response = new RF(ResponseType.success, question).createResponse();
      res.status(200).json(response);
    } catch (error) {
      next(error);
    }
  },
};
