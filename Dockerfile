FROM node:18-alpine
WORKDIR /tara
COPY . .
RUN npm install --verbose 
RUN npm install ts-node-dev ts-node dotenv-cli -g --verbose 
CMD ["npm", "run", "start"] 